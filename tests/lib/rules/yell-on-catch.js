/**
 * @fileoverview Choose Futures over Promises
 * @author brekk
 */
"use strict"

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/yell-on-catch")

const RuleTester = require(`eslint`).RuleTester
RuleTester.setDefaultConfig({
  parserOptions: {
    ecmaVersion: 6,
    sourceType: `module`
  }
})

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester()
ruleTester.run("yell-on-catch", rule, {
  valid: [
    {
      code: `new Future((bad, good) => x.then(good).catch(bad))`
    },
    {
      code: `new F((bad, good) => x.then(good).catch(bad))`,
      options: [{ library: "F" }]
    }
  ],

  invalid: [
    {
      code: "x.catch()",
      errors: [
        {
          message: 'Unexpected use of "catch"! Please use Futures instead.'
        }
      ]
    }
  ]
})
