/**
 * @fileoverview Choose Futures over Promises
 * @author brekk
 */
"use strict"

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require("../../../lib/rules/yell-on-then")

const RuleTester = require(`eslint`).RuleTester
RuleTester.setDefaultConfig({
  parserOptions: {
    ecmaVersion: 6,
    sourceType: `module`
  }
})

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const ruleTester = new RuleTester()
ruleTester.run("yell-on-then", rule, {
  valid: [
    {
      code: `new Future((bad, good) => x.then(good).catch(bad))`
    },
    {
      code: `new F((bad, good) => x.then(good).catch(bad))`,
      options: [{ library: "F" }]
    }
  ],

  invalid: [
    {
      code: "x.then()",
      errors: [
        {
          message: 'Unexpected use of "then"! Please use Futures instead.'
        }
      ]
    }
  ]
})
