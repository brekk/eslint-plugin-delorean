const { curry, reduce, pipe, isFunction, pathOr } = require("f-utility")
const NO_MATCH = `FUTILITY_NO_MATCHING`
const safePathWalk = curry((x, steps) =>
  reduce(
    (last, next) => {
      if (last && last[next]) return last[next]
      return NO_MATCH
    },
    x,
    steps
  )
)

const safePathOr = curry((alt, steps, x) =>
  pipe(
    safePathWalk(x),
    y => (y === NO_MATCH ? alt : y)
  )(steps)
)

const safePathEq = curry((compare, steps, x) =>
  pipe(
    safePathWalk(x),
    y => {
      if (y === NO_MATCH) return false
      return isFunction(compare) ? compare(y) : y === compare
    }
  )(steps)
)

const unexpected = curry(
  (x, Lib) => `Unexpected use of "${x}"! Please use ${Lib}s instead.`
)
const THEN_ERROR = unexpected(`then`)
const CATCH_ERROR = unexpected(`catch`)

module.exports = {
  unexpected,
  THEN_ERROR,
  CATCH_ERROR,
  safePathEq,
  safePathOr
}
