const { safePathEq, safePathOr, unexpected } = require("../utils")

/**
 * @fileoverview Choose Futures over Promises
 * @author brekk
 */

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    docs: {
      description: "Don't use Promise.catch, use Future.fork(bad, good)",
      category: "Async",
      recommended: false
    },
    fixable: null, // or "code" or "whitespace"
    // "yell-on-catch": [2, "never", {"library": "F"}]
    schema: [
      {
        properties: {
          library: {
            type: "string",
            description: 'An alternative library name to "Future"'
          }
        }
      }
    ]
  },

  create: function(context) {
    return {
      Identifier(node) {
        const LibraryName = safePathOr(
          "Future",
          ["options", 0, "library"],
          context
        )
        if (
          node &&
          node.name &&
          node.name === `catch` &&
          node.parent.type === `MemberExpression`
        ) {
          if (
            safePathEq(
              LibraryName,
              [
                `parent`,
                `parent`,
                `parent`,
                `parent`,
                `parent`,
                `expression`,
                `callee`,
                `name`
              ],
              node
            )
          ) {
            // ------------------------------v
            // new Future(b, g) => x.then(g).catch(b))
            // ----^
            return
          }
          context.report({
            node,
            message: unexpected(node.name, LibraryName),
            loc: node.loc
          })
          return
        }
      }
    }
  }
}
