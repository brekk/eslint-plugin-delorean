# eslint-plugin-delorean

Choose [Futures](https://github.com/fluture-js/Fluture/wiki/Comparison-to-Promises) over Promises

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-delorean`:

```
$ npm install eslint-plugin-delorean --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-delorean` globally.

## Usage

Add `delorean` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "delorean"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
        "delorean/yell-on-then": 2,
        "delorean/yell-on-catch": 2
    }
}
```

## Supported Rules

### yell-on-then

*Invalid Code*:
```
x.then()
```
*Valid Code*:
```
new Future((bad, good) => x.then(good).catch(bad))
```
*Options*:
```
{ "library": "F" }
```
* library - Optionally provide a library to validate against instead of "Future"

### yell-on-catch

*Invalid Code*:
```
x.catch()
```
*Valid Code*:
```
new Future((bad, good) => x.then(good).catch(bad))
```
*Options*:
```
{ "library": "F" }
```
* library - Optionally provide a library to validate against instead of "Future"




